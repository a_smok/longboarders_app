﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using boardly.data;
using boardly.Model;

namespace boardly
{
    public partial class MainPage : ContentPage
    {
        
        public MainPage()
        {
            
            InitializeComponent();
            var asembly = typeof(MainPage);

        }

        private async void LoginButton_Clicked (object sender, EventArgs e)
        {
           
            bool isEmailEmpty = string.IsNullOrEmpty(EmailEntry.Text);
            bool isPasswordEmpty = string.IsNullOrEmpty(PasswordEntry.Text);
            if(isEmailEmpty || isPasswordEmpty)
            {

            }
            else
            {
                var user = (await App.MobileService.GetTable<User>().Where(u => u.Email == EmailEntry.Text).ToListAsync()).FirstOrDefault();

                if(user != null)
                {
                    if (user.Password == PasswordEntry.Text)
                        await Navigation.PushAsync(new MapPage());
                    else
                        await DisplayAlert("Error", "Email or password is incorrect", "Ok");
                }
                else
                {
                    await DisplayAlert("Error", "Something went wrong,please try again", "OK");
                }
               
            }
        }

        private void RegisterUserButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegisterPage());
        }
    }
}
