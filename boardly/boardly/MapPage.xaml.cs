﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using boardly.data;
using Plugin.Geolocator;
using boardly.Model;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace boardly
{
    public partial class MapPage : ContentPage
    {
        
        
        private double LatTapped;
        private double LongTapped;

        public MapPage()
        {
           
            InitializeComponent();

            myMap.MyLocationEnabled = true;
            myMap.IsIndoorEnabled = true;
            myMap.UiSettings.CompassEnabled = true;
            myMap.UiSettings.RotateGesturesEnabled = true;
            myMap.UiSettings.MyLocationButtonEnabled = true;
            myMap.UiSettings.ScrollGesturesEnabled = true;
            myMap.UiSettings.IndoorLevelPickerEnabled = true;
            myMap.UiSettings.TiltGesturesEnabled = true;
            myMap.UiSettings.ZoomControlsEnabled = true;
            myMap.UiSettings.ZoomGesturesEnabled = true;

            myMap.MapLongClicked += (sender, e) =>
            {
                popupPinAdd.IsVisible = true;
                LatTapped = e.Point.Latitude;
                LongTapped = e.Point.Longitude;
             
            };
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                var status =await CrossPermissions.Current.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Location);
                if(status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                    {
                        await DisplayAlert("Permission needed", "We will have to access your location", "OK");
                    }
                    var results=await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                    if (results.ContainsKey(Permission.Location))
                    {
                        status = results[Permission.Location];
                    }
                }
                if (status == PermissionStatus.Granted)
                {

                    var locator = CrossGeolocator.Current;
                    locator.PositionChanged += Locator_PositionChanged;
                    await locator.StartListeningAsync(new TimeSpan(), 50);
                    var position = await locator.GetPositionAsync();

                    var center = new Position(position.Latitude, position.Longitude);
                    var span = new MapSpan(center, 4, 4);
                    myMap.MoveToRegion(span);
                    var spots = await App.MobileService.GetTable<Spot>().ToListAsync();
                    DisplayInMap(spots);
                }
                else
                {
                    await DisplayAlert("Permission denied", "You didn`t granted permission to access your location,we cannot proceed", "Ok");
                }
            }
            catch(Exception)
            {

            }
        }

        protected async override void OnDisappearing()
        {
            base.OnDisappearing();

            var locator = CrossGeolocator.Current;
            locator.PositionChanged -= Locator_PositionChanged;
            await locator.StopListeningAsync();
        }

        private void DisplayInMap(List<Spot> spots)
        {
            foreach(var spot in spots)
            {
                var position = new Position(spot.Lat, spot.Long);

                Pin pin = new Pin()
                {
                    Type=PinType.Place,
                    Position=position,
                    Label=spot.Name,
                    Address=spot.Description
                };
                myMap.Pins.Add(pin);
            }
        }

        private void DisplayInMap(Spot spot)
        {
            var position = new Position(spot.Lat, spot.Long);

            Pin pin = new Pin()
            {
                Type = PinType.Place,
                Position = position,
                Label = spot.Name,
                Address = spot.Description
            };
            myMap.Pins.Add(pin);
        }
        private void Locator_PositionChanged(object sender, Plugin.Geolocator.Abstractions.PositionEventArgs e)
        {
            var center = new Xamarin.Forms.GoogleMaps.Position(e.Position.Latitude, e.Position.Longitude);
            var span = new Xamarin.Forms.GoogleMaps.MapSpan(center, 4, 4);
            myMap.MoveToRegion(span);
        }

        private void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ProfilePage());
        }

        private async void AddSpot_Clicked(object sender, EventArgs e)
        {
            try
            {
                Spot NewSpot = new Spot()
                {
                    Name = spotName.Text,
                    Lat = LatTapped,
                    Long = LongTapped,
                    Description = spotdescription.Text
                };

                popupPinAdd.IsVisible = false;
                await App.MobileService.GetTable<Spot>().InsertAsync(NewSpot);
                DisplayInMap(NewSpot);
                await DisplayAlert("Success", "Spot added succesfully", "Ok");
            }
            catch(NullReferenceException nre)
            {
                await DisplayAlert("Success", "Spot added succesfully", "Ok");
            }
            catch (Exception ex)
            {
                await DisplayAlert("Success", "Spot added succesfully", "Ok");
            }
        }
    }
}
