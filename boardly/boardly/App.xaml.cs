﻿using Microsoft.WindowsAzure.MobileServices;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace boardly
{
    public partial class App : Application
    {
        public static string DatabasePath = string.Empty;
        public static MobileServiceClient MobileService =
            new MobileServiceClient(
            "https://boardlyapp.azurewebsites.net"
        );


        public App()
        {
            InitializeComponent();
            MainPage =new NavigationPage( new MainPage());
            
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
