﻿using boardly.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace boardly
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterPage : ContentPage
	{
		public RegisterPage ()
		{
			InitializeComponent ();
		}

        private async void RegisterButton_Clicked(object sender, EventArgs e)
        {
            if(PasswordEntry.Text == ConfirmPasswordEntry.Text)
            {
                User user = new User()
                {
                    Email = EmailEntry.Text,
                    Password = PasswordEntry.Text,
                };

                await App.MobileService.GetTable<User>().InsertAsync(user);
                await DisplayAlert("Successs", "Registration succeeded", "Ok");
            }
            else
            {
                await DisplayAlert("Error", "Passwords don`t match", "Ok");
            }
        }
    }
}